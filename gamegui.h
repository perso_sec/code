#ifndef GAMEGUI_H
#define GAMEGUI_H

#include <irrlicht.h>
#include "enums.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class GameGUI
{
    public:
        GameGUI(IGUIEnvironment *guienv);
        virtual ~GameGUI();
        //Var
        IGUIEnvironment *guienv;
        //Fen�tre du pendu;
        IGUIStaticText  *FenetrePendu;
        IGUIStaticText  *TitreFenetrePendu;
        IGUIButton      *btnNouvellePartie;
        //Fen�tre JEU
        IGUIStaticText  *FenetreJEU;
        IGUIStaticText  *TitreFenetreJeu;
        IGUIButton      *btnRecommencer;
        IGUIButton      *btnVerifLettre;
        IGUIStaticText  *FenetreDuPendu;
        IGUIStaticText  *LabelSaisir;
        IGUIEditBox     *BoxSaisie;
        IGUIStaticText  *LabelChance;
        IGUIStaticText  *LabelMotSecret;
        IGUIStaticText  *LabelAfficheChance;
        IGUIStaticText  *LabelInfo;
    protected:

    private:

};

#endif // GAMEGUI_H
