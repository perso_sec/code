#include "Game.h"

#include <time.h>

int main()
{
    srand(time(NULL));
    //Classe principale du jeu
	Game *P3Game = new Game();

	//Boucle principale
    P3Game->run();

    //Destructeur
    delete P3Game;

    return 0;
}
