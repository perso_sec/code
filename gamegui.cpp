#include "GameGUI.h"

GameGUI::GameGUI(IGUIEnvironment *guienv)
{
    //ctor
    this->guienv = guienv;
    //*********************************************************************************************
    //Fenetre Pendu
    //*********************************************************************************************
    FenetrePendu = guienv->addStaticText(L"", rect<int>(0,0,564,480));
    FenetrePendu->setBackgroundColor(video::SColor (180,255,127,80));
    FenetrePendu->setVisible(true);
    //Labels
    TitreFenetrePendu = guienv->addStaticText(L"L E   P E N D U", rect<int>(282-200,20,282+200,20+60), false, false, FenetrePendu);
    TitreFenetrePendu->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    TitreFenetrePendu->setBackgroundColor(video::SColor (255,255,215,0));
    //Boutons
    btnNouvellePartie = guienv->addButton(rect<int>(282-150,200,282-50,200+100), FenetrePendu, GUI_ID_BTN_NOUVEAU_JEU, L"NOUVEAU JEU");
    //*********************************************************************************************

    //*********************************************************************************************
    //Fenetre JEU
    //*********************************************************************************************
    FenetreJEU = guienv->addStaticText(L"", rect<int>(0,0,564,480));
    FenetreJEU->setBackgroundColor(video::SColor (180,255,127,80));
    FenetreJEU->setVisible(false);
    //Labels
    TitreFenetreJeu = guienv->addStaticText(L"L E   P E N D U", rect<int>(282-200,20,282+200,20+60), false, false, FenetreJEU);
    TitreFenetreJeu->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    TitreFenetreJeu->setBackgroundColor(video::SColor (255,255,215,0));
    FenetreDuPendu = guienv->addStaticText(L"", rect<int>(282,100,282+250,100+200), false, false, FenetreJEU);
    FenetreDuPendu->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    FenetreDuPendu->setBackgroundColor(video::SColor (255,255,215,0));
    LabelSaisir = guienv->addStaticText(L"Saisir une lettre", rect<int>(282+10,100+10,282+115,100+40), true, false, FenetreJEU);
    LabelSaisir->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    LabelSaisir->setBackgroundColor(video::SColor (255,255,99,71));
    LabelMotSecret = guienv->addStaticText(L"", rect<int>(282+60,100+50,282+180,100+90), true, false, FenetreJEU);
    LabelMotSecret->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    LabelMotSecret->setBackgroundColor(video::SColor (255,255,99,71));
    LabelChance = guienv->addStaticText(L"Chances restantes", rect<int>(282+10,100+150,282+115,100+190), true, false, FenetreJEU);
    LabelChance->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    LabelChance->setBackgroundColor(video::SColor (255,255,99,71));
    LabelAfficheChance = guienv->addStaticText(L"", rect<int>(282+10+120,100+150,282+240,100+190), true, false, FenetreJEU);
    LabelAfficheChance->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    LabelAfficheChance->setBackgroundColor(video::SColor (255,255,99,71));
    LabelInfo = guienv->addStaticText(L"", rect<int>(282,100+220,282+250,100+260), false, false, FenetreJEU);
    LabelInfo->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    LabelInfo->setBackgroundColor(video::SColor (255,255,215,0));
    //EditBox
    BoxSaisie = guienv->addEditBox(L"", rect<int>(282+10+120,100+10,282+240,100+40), true, FenetreJEU, GUI_ID_EDIT_SAISIE);
    BoxSaisie->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER );
    BoxSaisie->setOverrideColor(video::SColor (255,255,99,71));
    //Boutons
    btnRecommencer = guienv->addButton(rect<int>(282-150,200,282-50,200+100), FenetreJEU, GUI_ID_BTN_RECOMMENCER, L"RECOMMENCER");
    btnVerifLettre = guienv->addButton(rect<int>(282+60,100+100,282+180,100+140), FenetreJEU, GUI_ID_BTN_VERIF_SAISIE, L"VERIFIER SAISIE");
    //*********************************************************************************************
}

GameGUI::~GameGUI()
{
    //dtor
}



