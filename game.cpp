#include "Game.h"
#include <iostream>
#include <dirent.h>

#include <ctime>
#include <fstream>

Game::Game()
{
    //ctor
    //Creating device for Irrlicht
    device = createDevice(EDT_SOFTWARE, dimension2d<u32>(564, 480), 16,false, false, false, 0);
    driver = device->getVideoDriver();
    guienv = device->getGUIEnvironment();

    //Craeting the GUI class
    gui = new GameGUI(guienv);

    //Event receiver
    receiver = new MyEventReceiver(this);

    //Link between the device and the envent receiver
    device->setEventReceiver(receiver);

    //var
    status = PENDU_STATUS_EN_ATTENTE;
    chance = 7;
    gagne=false;
    tempText = L"";
    motJeu = "";
}

Game::~Game()
{
    //dtor
    device->drop();
}

void Game::run()
{
    // In order to do framerate independent movement, we have to know
    // how long it was since the last frame
    u32 then = device->getTimer()->getTime();//I

    //initialising
    //FPS
    int lastFPS = -1;

    //Chargement des mots du fichier dans un vecteur
    int i=0;
    std::ifstream file(".\\mots.txt");
    std::string str;
    while (std::getline(file, str))
    {
        vMots.push_back(str);
        ++i;
    }

    //chargement du premier mot pour le jeu
    reset();

    //Main Loop
    while (device->run())
    {
        // Work out a frame delta time.
        const u32 now = device->getTimer()->getTime();
        frameDeltaTime = (f32)(now - then) / 1000.f; // Time in seconds
        then = now;

        //Traitement de l editbox
        if(getStatus()==PENDU_STATUS_EN_COURS)
        {
            gui->BoxSaisie->setVisible(true);
        }
        else if (getStatus()==PENDU_STATUS_FINI)
        {
            gui->BoxSaisie->setVisible(false);
        }

        //Render Loop
        driver->beginScene(true, true, SColor(0,200,200,200));
        guienv->drawAll();
        driver->endScene();

        //Display/update FPS from the windows' bar
        int fps = driver->getFPS();
        if (lastFPS != fps)
        {
            core::stringw tmp(L"Game of the pendu | fps: ");
            tmp += fps;
            device->setWindowCaption(tmp.c_str());
            lastFPS = fps;
        }
    }
}

//F/M
void Game::reset()
{
    //on vide le vecteur
    vMotJeu.clear();
    affichageMotSecret = L"";
    //S�lection d'un mot dans le vecteur vMots
    motJeu = vMots[rand()%vMots.size()];
    //remplissage du vecteur de v�rification
    for (unsigned int i = 0; i<motJeu.size();++i)
    {
        vMotJeu.push_back(0);
        affichageMotSecret += "*";
    }
    //re/initialisation des variables et labels
    gagne = false;
    chance = 7;
    gui->LabelInfo->setText(L"");
    tempText = L"";
    tempText += chance;
    gui->LabelAfficheChance->setText(tempText.c_str());
    gui->BoxSaisie->setText(L"");
    gui->LabelMotSecret->setText(affichageMotSecret.c_str());
}

void Game::checkSaisie()
{
    //on v�rifie si on a bien saisi 1 lettre
    irr::core::stringw temp = gui->BoxSaisie->getText();
    if (temp.size()==1)
    {
        //On v�rifie si la lettre est dans le mot
        std::string tempString = core::stringc( gui->BoxSaisie->getText() ).c_str();
        std::size_t found = motJeu.find(tempString);
        if (found!=std::string::npos)
        {
            gagne = true;
            //si elle s'y trouve, on modifie le vecteur de v�rification
            for (unsigned int i = 0; i<motJeu.size();++i)
            {
                if(motJeu[i]==temp[0])
                {
                    vMotJeu[i]=1;
                    affichageMotSecret[i] = temp[0];
                }
                //on en profite pour v�rifier s'il reste des lettres non trouv�es dans le vecteur
                if (vMotJeu[i]==0)
                {
                    gagne=false;
                }
            }
            gui->LabelMotSecret->setText(affichageMotSecret.c_str());
        }
        //sinon, on enl�ve une chance
        else
        {
            --chance;
            tempText = L"";
            tempText += chance;
            gui->LabelAfficheChance->setText(tempText.c_str());
        }
        //reinitialisation message d'erreur et box de saisie
        gui->LabelInfo->setText(L"");
        gui->BoxSaisie->setText(L"");
    }
    else if(temp.size()!=0)
    {
        //affichage du message d'erreur
        gui->LabelInfo->setText(L"ERREUR SAISIE : NE SAISIR QU'UNE LETTRE");
        gui->BoxSaisie->setText(L"");
    }
    //Partie finie?
    verifResultat();
}

void Game::verifResultat()
{
    //v�rification de fin de partie (gang�e ou perdue)
    if (chance==0 || gagne==true)
    {
        status = PENDU_STATUS_FINI;
        if (chance == 0)
        {
            //perdu
            gui->LabelInfo->setText(L"PAS DE CHANCE !");
        }
        else
        {
            //gagn�
            gui->LabelInfo->setText(L"BRAVO !");
        }
    }
}

//get
int Game::getStatus()
{
    return this->status;
}

//set
void Game::setStatus(int status)
{
    this->status = status;
}

