#ifndef ENUM_H
#define ENUM_H

enum
{
    //Fenetre Pendu
    GUI_ID_BTN_NOUVEAU_JEU=101,
    //Fenetre JEU
    GUI_ID_BTN_RECOMMENCER,
    GUI_ID_BTN_VERIF_SAISIE,
    GUI_ID_EDIT_SAISIE,
    //Status du jeu
    PENDU_STATUS_EN_ATTENTE,
    PENDU_STATUS_EN_COURS,
    PENDU_STATUS_FINI,
};

#endif
