#include "MyEventReceiver.h"
#include "game.h"

#include <iostream>

MyEventReceiver::MyEventReceiver(Game *P3Game)
{
    //ctor
    this->P3Game = P3Game;
}

MyEventReceiver::~MyEventReceiver()
{
    //dtor
}

bool MyEventReceiver::OnEvent(const SEvent& event)
{
    if (event.EventType == EET_GUI_EVENT)
    {
        s32 id = event.GUIEvent.Caller->getID();

        switch(event.GUIEvent.EventType)
        {
        case EGET_EDITBOX_ENTER:
            switch (id)
            {
                case GUI_ID_EDIT_SAISIE:
                //Verification de la saisie editbox
                P3Game->checkSaisie();
                return true;
            default:
                return false;
            }

        case EGET_BUTTON_CLICKED:
            switch(id)
            {
            case GUI_ID_BTN_NOUVEAU_JEU:
                //changer le status du jeu
                P3Game->gui->FenetreJEU->setVisible(true);
                P3Game->gui->FenetrePendu->setVisible(false);
                //afficher la fenetre de jeu
                P3Game->setStatus(PENDU_STATUS_EN_COURS);
                return true;
            case GUI_ID_BTN_RECOMMENCER:
                //reset du jeu
                P3Game->reset();
                P3Game->setStatus(PENDU_STATUS_EN_COURS);
            case GUI_ID_BTN_VERIF_SAISIE:
                //Verification de la saisie editbox
                P3Game->checkSaisie();
                return true;
            default:
                return false;
            }
        default:
            break;
        }
    }
    return false;
}
