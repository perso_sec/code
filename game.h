#ifndef GAME_H
#define GAME_H

#include <irrlicht.h>
#include <string>
#include <vector>

#include "GameGUI.h"
#include "MyEventReceiver.h"


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class Game
{
    public:
        Game();
        virtual ~Game();

        //var
        GameGUI *gui;
        IGUIEnvironment *guienv;
        f32 frameDeltaTime;

        //Methods/functions
        void run();
        void reset();
        void checkSaisie();
        void verifResultat();

        //get
        int getStatus();

        //set
        void setStatus(int status);
    protected:

    private:
        //Irrlicht
        IrrlichtDevice *device;
        IVideoDriver *driver;
        MyEventReceiver *receiver;
        //Jeu
        int status;
        std::vector<std::string> vMots;
        int chance;
        std::string motJeu;
        std::vector<int> vMotJeu;
        bool gagne;
        core::stringw tempText;
        core::stringw affichageMotSecret;
};

#endif // GAME_H
